package com.example.sensorapp;

public class ByteHelper {

    private static boolean isLetterOrDigit(char c)
    {
        return (c >= 'a' && c <= 'z') ||
                (c >= 'A' && c <= 'Z') ||
                (c >= '0' && c <= '9');
    }

    static public String byteToString(byte[] packetBytes)
    {
        String s = new String(packetBytes);
        String data = new String();

        for (int i = 0; i < s.length(); i++)
        {
            if(isLetterOrDigit(s.charAt(i)))
            {
                data = data + s.charAt(i);
            }
        }

        return data;
    }


}
