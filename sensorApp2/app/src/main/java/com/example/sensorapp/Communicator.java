package com.example.sensorapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public class Communicator extends AsyncTask<Integer, Integer, Integer>{

     public final int DEFAULT_SLEEP = 100;
     public final int DEFAULT_BUFFER_LENGTH = 100;

     public static Menu menu = null;
     public static boolean isPaused = true;
     public static boolean isConnected = false;
     public static TimeIRL timeIRL = null;

    public static TextView distanceTextView;
    public static ProgressDialog progressDialog;

     final String UPDATE_SETTINGS_MODE = "3";
     final String WRITE_MODE = "2";
     final String DISPLAY_MODE = "1";
     final String START = "4";
     final String STOP = "5";

     public boolean stop = false;
     OutputStream outputStream;
     InputStream inStream;
     BluetoothAdapter blueAdapter;
     BluetoothSocket socket;


     Context context1;
     ImageView bluetoothStatus;
     private ImageView item;
     private boolean active;

     private Activity MainActivity;
     public String bluetoothName;
     public String units;


    public Communicator(Activity activity, String bluetoothName, String units)
    {
        this.units = units;
        this.MainActivity = activity;
        this.bluetoothName = bluetoothName;

        distanceTextView = activity.findViewById(R.id.text);
        context1 = activity.getApplicationContext();
        bluetoothStatus = MainActivity.findViewById(R.id.bluetooth_status);
        blueAdapter = BluetoothAdapter.getDefaultAdapter();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        MainActivity.registerReceiver(mReceiver, filter);

        }



        /*
         *
         * run in the background and wait for data
         *
         * */
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Integer doInBackground(Integer... integers) {

            String data = "";
            connect(bluetoothName);

            /* pause loop */
            while(true)
            {
                while(Communicator.isPaused)
                {
                    sleep(DEFAULT_SLEEP);
                    Log.d("DataThread", "wait");

                    if(this.stop)
                        return null;
                }

                if(this.stop)
                    break;


            /* data loop */
            try {
                if(inStream == null || inStream.available() == 0 || !isConnected)
                   continue;

                data = readData();
                Log.e("DataDataDataDataData", data);

                switch (Parser.getState(data))
                {
                    case DISPLAY_MODE:
                        displayMode(data);
                        break;

                    case WRITE_MODE:
                        TimeIRL currTimeIRL = new TimeIRL(timeIRL);
                        currTimeIRL.addSeconds(Float.valueOf(Parser.getTime(data)));
                        FileManager.writeOutputFile(Parser.getTime(data), currTimeIRL.toString(), parseData(data));
                        break;

                    case START:
                        MainActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(MainActivity.getApplicationContext(), "Downloading", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;

                    case STOP:
                        MainActivity.runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(MainActivity.getApplicationContext(), "Done Downloading", Toast.LENGTH_SHORT).show();
                            }
                        });
                        break;
                }

            } catch (Exception e) {
                Log.d("ErrorThread", e.toString());
            }
        }

        return null;
    }



    private String displayMode(String data)
    {

        try {

            String distanceString = parseData(data);
            Log.e("DataDataDataData", distanceString);
            if (distanceString != "null" && distanceString != "")
                distanceTextView.setText(distanceString);

        } catch (Exception e) {
            Log.d("ErrorReadMode", e.toString());
        }

        return data;

    };

    private String readData()
    {

        String data = "";
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {

            byte[] packetBytes = new byte[DEFAULT_BUFFER_LENGTH];
            byte[] packetBytesEnd;

            while (inStream.available() != 0)
            {
                inStream.read(packetBytes);
                output.write(packetBytes);
            }


            packetBytesEnd = output.toByteArray();
            data = ByteHelper.byteToString(packetBytesEnd);


        } catch (Exception e) {
            Log.d("ErrorReadData", e.toString());
        }

        return data;

    }


    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {


        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);


            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                if (menu != null)
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(MainActivity, R.drawable.icon_connected));

                isConnected = true;
                Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();

            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                if (menu != null)
                    menu.getItem(0).setIcon(ContextCompat.getDrawable(MainActivity, R.drawable.icon_disconnected));

                Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT).show();
                isConnected = true;
            }
        }
    };

     private String parseData(String data)
    {

        if (data == "")
            return "";

        String distanceString = Parser.getDistance(data);

        float f = Float.parseFloat(distanceString);

        if (units.compareTo("cm") == 0)
        {
            f = f / 10;
        }


        else if (units.compareTo("m") == 0)
        {
            f = f / 1000;
        }




        distanceString = String.valueOf(f)  + units;
        return distanceString;
    }

    /*
     *
     * called when the right device have bin found
     *
     * connect to the device and start the socket
     *
     * */
     public void init(BluetoothDevice newDevice)
    {
        try {
            
            Set<BluetoothDevice> bondedDevices = blueAdapter.getBondedDevices();

            if (bondedDevices.size() > 0) {

                ParcelUuid[] uuids = newDevice.getUuids();
                socket = newDevice.createRfcommSocketToServiceRecord(uuids[0].getUuid());
                socket.connect();

                outputStream = socket.getOutputStream();
                inStream = socket.getInputStream();

                this.resume();
            }

        } catch (Exception e) {
            Log.d("ErrorInit", e.toString());
        }



    }

    /* reconnect to */
     public void reconnect(String bluetoothName)
    {
        connect(bluetoothName);

    }

     public void write(String s)
    {
        try {
            outputStream.write(s.getBytes());
        } catch (Exception e) {
            Log.d("Error", e.toString());
        }
    }

    /*
     *
     * find the device with right name
     *
     * */
     private void connect(String bluetoothName)
    {
        boolean flag = true;

        // get all the device's
        Set<BluetoothDevice> pairedDevices = blueAdapter.getBondedDevices();

        Log.e("DataDataa", bluetoothName);

        if (pairedDevices.size() > 0) {

            // find the right one
            for (BluetoothDevice device : pairedDevices) {

                String deviceName = device.getName();

                Log.e("DataData", deviceName);

                if (deviceName.equals(bluetoothName))
                {
                    flag = false;

                    // connect to it
                    this.init(device);
                }
            }

            if(flag)
                Log.e("ErrorConnect", "aaaa");

        }
        else
            Log.e("ErrorConnect", "aaaa");


    }

    /*
     *
     * close the connection
     *
     * */
     public void disconnect()
    {
        try {

            this.inStream = null;
            this.outputStream = null;
            if (this.socket != null || this.socket.isConnected())
                this.socket.close();

        } catch (IOException e) {
            Log.d("ErrorDisconnect", e.toString());
        }
    }

     public void pause()
    {
        this.isPaused = true;
    }

     public void resume()
    {
        this.isPaused = false;
    }

     public void sleep(long sleepDuration)
    {
        try
        {
            Thread.sleep(sleepDuration);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

}
