package com.example.sensorapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private final String SETTINGS_FILE = "settings.txt";
    private final String DEFAULT_BLUETOOTH_NAME = "ESP32test";
    private final String DEFAULT_TIME = "1";
    private final String DEFAULT_FILE_NAME = "test";
    private final String DEFAULT_UNITS = "mm";

    public final int REQUEST_ENABLE_BT = 2;

    private BluetoothAdapter bluetoothAdapter;
    static public Communicator communicator = null;

    private Button onOffBtn;
    private ImageView clearImage;
    private ImageView sherImage;
    private ImageView downloadImage;

    private boolean active = false;

    private String time;
    private String path;
    private String units;
    private String bluetoothName;

    private boolean kill = true;
    public Menu menu;
    ProgressDialog progressDialog;

    public MainActivity() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        Communicator.menu = menu;
        if (communicator != null && Communicator.isConnected)
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.icon_connected));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.settings:
                clickMenu();
                return true;

            case R.id.bluetooth_status:
                if(communicator != null)
                communicator.reconnect(bluetoothName);
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                1);

        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                2);

        setContentView(R.layout.activity_main);

        onOffBtn = findViewById(R.id.on_off_btn);
        clearImage = findViewById(R.id.clear_img);
        sherImage = findViewById(R.id.shere_img);
        downloadImage = findViewById(R.id.download);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Data from sensor");
        progressDialog.setMessage("Downloading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        try
        {

            getSettings();
            FileManager.init(path, getApplicationContext());

            Log.e("DataData", bluetoothName);
            Log.e("DataData", time);
            Log.e("DataData", path);

            // run the thread the send the receive the data
            if (communicator == null)
                communicator = (Communicator) new Communicator(this, bluetoothName, units).execute(0, 0, 0);

            communicator.bluetoothName = bluetoothName;
            communicator.units = units;
            Communicator.distanceTextView = findViewById(R.id.text);

            kill = true;

            setUpBluetooth();

        }catch (Exception e)
        {
            Log.d("ErrorOnCreate", e.toString());
        }

    }

    /*
    *
    * Load the settings and update the put the values into Time Path and bluetoothName
    *
    * */
    private void getSettings()
    {
        try
        {
            // get the settings
            String settingsJsonString = FileManager.readFromFile(getApplicationContext(), SETTINGS_FILE);
            Log.e("DataData", settingsJsonString);

            // check if there are settings and if not initialize them with the default one
            if (settingsJsonString == null || settingsJsonString.equals(""))
            {
                JSONObject jsonObject = new JSONObject();

                bluetoothName = DEFAULT_BLUETOOTH_NAME;
                time = DEFAULT_TIME;
                path = "test";

                jsonObject.put("bluetoothName", DEFAULT_BLUETOOTH_NAME);
                jsonObject.put("Time", DEFAULT_TIME);
                jsonObject.put("Path", DEFAULT_FILE_NAME);
                jsonObject.put("Units", DEFAULT_UNITS);

                String userString = jsonObject.toString();

                FileManager.writeToFile(getApplicationContext(),SETTINGS_FILE, userString);

            }

            // if there are settings get them
            else
            {
                JSONObject settingsJson  = new JSONObject(settingsJsonString);

                bluetoothName = settingsJson.getString("bluetoothName");
                time = settingsJson.getString("Time");
                path = settingsJson.getString("Path");
                units = settingsJson.getString("Units");
            }

        }catch (Exception e)
        {
            Log.e("ErrorLoadSettings", e.toString());
        }

    }

    public void onOffButtonClick(View view)
    {

        if(Communicator.isPaused)
            return;

        active = !active;

        if (active)
            this.clickOn();
        else
            this.clickOff();
    }

    /*
     *
     * called when the user click on the on button
     *
     * send request for the esp32 to send data
     *
     * */
    public void clickOn()
    {
        if(!Communicator.isConnected)
            return;


        JSONObject msg = new JSONObject();

        try
        {
            msg.put("state", "ON");
            msg.put("time", time);
        }
        catch (Exception e)
        {
            Log.d("ErrorOnclickOn", e.toString());
        }

        communicator.write(msg.toString());

        if (communicator.timeIRL == null)
            communicator.timeIRL = new TimeIRL(Calendar.getInstance());

        communicator.resume();
        Log.d("DataData", msg.toString());
        changeState();

    }

    /*
     *
     * called when the user click on the clear button
     *
     * send request for the esp32 to clear the data in the esp32
     *
     * */
    public void clearFile(View view)
    {
        if(!Communicator.isConnected)
            return;

        communicator.timeIRL = null;

        JSONObject msg = new JSONObject();

        try
        {
            msg.put("state", "clear");
            msg.put("time", time);
        }
        catch (Exception e)
        {
            Log.d("ErrorOnclickOn", e.toString());
        }

        communicator.write(msg.toString());
        Log.d("DataData", msg.toString());
        onOffBtn.setBackgroundResource(R.drawable.icon_start);
    }

    /*
     *
     * called when the user click on the clear button
     *
     * send request for the esp32 to clear the data in the esp32
     *
     * */
    public void getFile(View view)
    {
        if(!Communicator.isConnected)
            return;

        JSONObject msg = new JSONObject();

        try
        {
            msg.put("state", "download");
            msg.put("time", time);
        }
        catch (Exception e)
        {
            Log.d("ErrorOnclickOn", e.toString());
        }

        communicator.write(msg.toString());
        Log.d("DataData", msg.toString());
    }

    /*
     *
     * called when the user click on the off button
     *
     * send request for the esp32 to stop sending data
     *
     * */
    public void clickOff()
    {
        if(!Communicator.isConnected)
            return;

        JSONObject msg = new JSONObject();
        try
        {
            msg.put("state", "OFF");
            msg.put("time", "");
        }
        catch (Exception e)
        {
            Log.d("ErrorOnClickOff", e.toString());
        }

        communicator.write(msg.toString());
        changeState();
        Log.d("DataData", String.valueOf(msg));
    }

    /*
    *
    * change the the active icon picture if the variable active is true
    *
    * */
    private void changeState()
    {
        if (active)
        {
            onOffBtn.setBackgroundResource(R.drawable.icon_stop);
        }
        else
        {
            onOffBtn.setBackgroundResource(R.drawable.icon_start);
        }
    }

    /*
     *
     * called in onCreate
     *
     * check that the bluetooth is enabled
     *
     * */
    private void setUpBluetooth()
    {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null)
            Log.d("Error", "dose not support bluetooth");

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    /*
    *
    * called when the user click on the menu button
    *
    * open the menu activity
    *
    * */
    private void clickMenu()
    {
        //clickOff();
        communicator.pause();
        kill = false;
        //communicator.disconnect();


        Intent settingsIntent = new Intent(MainActivity.this, Settings.class);
        startActivity(settingsIntent);
    }

    public void sherFile(View view)
    {
        try
        {
            File root = getApplicationContext().getExternalFilesDir(null);

            File file = new File(root, path + ".csv");

            Intent intentSher = new Intent(Intent.ACTION_SEND);
            intentSher.setType("application/csv");
            intentSher.putExtra(Intent.EXTRA_STREAM, Uri.parse("" + file));
            startActivity(Intent.createChooser(intentSher, "Shara the file"));

        }catch (Exception e)
        {
            Log.e("ErrorSherFile", e.toString());
        }

    }

    @Override
    public void onPause() {
        super.onPause();

        communicator.pause();

    }

    @Override
    public void onResume()
    {
        super.onResume();
        communicator.resume();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        JSONObject msg = new JSONObject();
        try
        {
            msg.put("state", "OFF");
            msg.put("time", "");
        }
        catch (Exception e)
        {
            Log.d("Error", e.toString());
        }

        communicator.write(msg.toString());

        if (kill)
            communicator.disconnect();
    }



}

