package com.example.sensorapp;


import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

    static public String getState(String text)
    {

        String a = null;
        Pattern pattern = Pattern.compile("(?<=state).*$", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
               a = matcher.group();
        }

        a = a.replaceAll("[^\\d.]", "");
        return a;
    }

    static public String getTime(String text)
    {

        String a = null;
        Pattern pattern = Pattern.compile("(?<=Time).*?(?=dis)", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            a = matcher.group();
        }

        a = a.replaceAll("[^\\d.]", "");

        float f = Float.parseFloat(a);
        f = f / 1000;

        return String.valueOf(f);
    }

    static public String getDistance(String text)
    {

        String a = null;
        Pattern pattern = Pattern.compile("(?<=dis).*?(?=state)", Pattern.DOTALL);
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            a = matcher.group();
        }

        a = a.replaceAll("[^\\d.]", "");
        return a;
    }


}
