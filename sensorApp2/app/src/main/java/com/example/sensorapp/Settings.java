package com.example.sensorapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

public class Settings extends AppCompatActivity {

    public final String SETTINGS_FILE = "settings.txt";

    private String currUnit;
    private EditText timeEditText;
    private EditText nameEditText;
    private EditText pathEditText;

    private Spinner spinner;
    private static final String[] units = {"mm", "cm", "m"};

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.backIcon:
                backToMain();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);



        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(Settings.this, android.R.layout.simple_spinner_item, units);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currUnit = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                currUnit = units[0];
                spinner.setSelection(0);
            }
        });


        timeEditText = findViewById(R.id.timeEditText2);
        nameEditText = findViewById(R.id.timeEditText3);
        pathEditText = findViewById(R.id.TimeEditText);

        // get the settings
        String jsonString = FileManager.readFromFile(getApplicationContext(), SETTINGS_FILE);

        // initialize the textView's with the current settings
        try {

            JSONObject jsonObject  = new JSONObject(jsonString);
            nameEditText.setText(jsonObject.getString("bluetoothName"));
            timeEditText.setText(jsonObject.getString("Time"));
            pathEditText.setText(jsonObject.getString("Path"));
            setSpinnerByValue(jsonObject.getString("Units"));

        } catch (JSONException e) {
            Log.e("ErrorJason", e.toString());
        }
    }




    /*
     *
     * called when the user click on the back to menu button
     *
     * open the mainActivity
     *
     * */
    private void backToMain()
    {
        Intent settingsIntent = new Intent(Settings.this, MainActivity.class);

        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("Time", timeEditText.getText());
            jsonObject.put("bluetoothName", nameEditText.getText());
            jsonObject.put("Path", pathEditText.getText());
            jsonObject.put("Units", currUnit);

        } catch (JSONException e) {
            Log.e("ErrorJason", e.toString());
        }

        String userString = jsonObject.toString();
        FileManager.writeToFile(getApplicationContext(),SETTINGS_FILE, userString);

        startActivity(settingsIntent);
    }

    private void setSpinnerByValue(String value)
    {
        for (int i = 0; i < units.length; i++)
        {
            if (value.compareTo(units[i]) == 0)
                spinner.setSelection(i);
        }
    }


}