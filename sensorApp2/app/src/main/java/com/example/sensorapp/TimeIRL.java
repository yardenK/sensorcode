package com.example.sensorapp;

import java.util.Calendar;

public class TimeIRL {

    private final int MAX_SECONDS = 60;
    private final int MAX_MINUTES = 60;
    private final int MAX_HOURS = 24;

    private Calendar calendar;
    private int hour24hrs;
    private int hour12hrs;
    private int minutes;
    private float seconds;
    private float milliseconds;

    public TimeIRL(Calendar calendar)
    {
        calendar = calendar;
        hour24hrs = calendar.get(Calendar.HOUR_OF_DAY);
        hour12hrs = calendar.get(Calendar.HOUR);
        minutes = calendar.get(Calendar.MINUTE);
        seconds = calendar.get(Calendar.SECOND);
        milliseconds = 0;
    }

    public TimeIRL(TimeIRL timeIRL)
    {
        calendar = calendar;
        hour24hrs = timeIRL.getHour24hrs();
        hour12hrs = timeIRL.getHour12hrs();
        minutes = timeIRL.getMinutes();
        seconds = timeIRL.getSeconds();
        milliseconds = 0;
    }



    @Override
    public String toString() {
        return
                ": hours=" + hour24hrs +
                ": minutes=" + minutes +
                ": seconds=" + seconds +
                '}';
    }

    void addSeconds(float addSeconds)
    {
        seconds = seconds + addSeconds;

        while(seconds >= MAX_SECONDS)
        {
            seconds = seconds - MAX_SECONDS;
            addMinutes(1);
        }
    }

    void addMinutes(int addMinutes)
    {
        minutes = minutes + addMinutes;

        while(minutes >= MAX_MINUTES)
        {
            minutes = minutes - MAX_MINUTES;
            addHours(1);
        }
    }

    void addHours(int addHours)
    {
        hour24hrs = hour24hrs + addHours;

        while(hour24hrs >= MAX_HOURS)
        {
            minutes = 0;
        }
    }

















    public void setMilliseconds(float milliseconds) {
        this.milliseconds = milliseconds;
    }

    public float getMilliseconds() {
        return milliseconds;
    }

    public int getHour24hrs() {
        return this.hour24hrs;
    }

    public void setHour24hrs(int hour24hrs) {
        this.hour24hrs = hour24hrs;
    }

    public int getHour12hrs() {
        return hour12hrs;
    }

    public void setHour12hrs(int hour12hrs) {
        this.hour12hrs = hour12hrs;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public float getSeconds() {
        return seconds;
    }

    public void setSeconds(float seconds) {
        this.seconds = seconds;
    }
}
