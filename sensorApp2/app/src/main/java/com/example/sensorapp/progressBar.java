package com.example.sensorapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

public class progressBar {

    private Activity activity;
    private AlertDialog alertDialog;

    progressBar(Activity activity)
    {
        this.activity = activity;
    }

    public void show()
    {
       AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = activity.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.dialog, null));
        builder.setCancelable(false);

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void stop()
    {
        alertDialog.dismiss();
    }

}
