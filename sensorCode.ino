#include "BluetoothSerial.h"
#include <ArduinoJson.h>

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

#define ON 1
#define OFF 0

#define RX 16
#define TX 17

#define HEADER 250
#define HEADER_STATE 0
#define DIS_L_STATE 1
#define DIS_H_STATE 2
#define ERROR_STATE 3
#define CHECK_SUM_STATE 4
#define END_STATE 5
#define ONE_SEC 1000
#define DEFAULT_TIME 1
#define DEFAULT_BUFFER_SIZE 1000
#define DEFAULT_JSON_BUFFER_SIZE 1024
#define BAUD 230400

DynamicJsonDocument doc(DEFAULT_JSON_BUFFER_SIZE);
BluetoothSerial SerialBT;

float checkTime = 1;
long lastMillis = 0;
int active = OFF;
long previousMillis = 0;
int counter = 0;

int state = 0;
int header = 0;
byte dis_l = 0;
byte dis_h = 0;
int err = 0;
int checkSum = 0;
int dis = 0;

int good = 0;
int bad = 0;

struct Data {
  char  title[30];
  char  author[30];
};

Data dataArr[30000];

unsigned long StartTime = 0;

void setup()
{
  Serial.begin(BAUD);
  SerialBT.begin("ESP32test");
  Serial2.begin(BAUD, SERIAL_8N1, RX, TX);
  StartTime = millis();
}

void loop() 
{
  unsigned long currentMillis = millis();

  while(!Serial2.available()); // wait for the serial port to senr data
  {
     byte readByte = Serial2.read();
     initData(readByte);
  }
   
  if (SerialBT.available()) {
    
    deserializeJson(doc, SerialBT.readString());
    JsonObject obj = doc.as<JsonObject>();
    
    const char* state = obj["state"];
    const char* checkTimeString = obj["time"];

    checkState(state, checkTimeString);
  }

  if(currentMillis - previousMillis >= ONE_SEC * checkTime)
  {
    if(active == ON)
    {
      counter++;
      sendData();
    }
    
    previousMillis = currentMillis;     
  }    
}



void sendData()
{
  char returnTime[DEFAULT_BUFFER_SIZE] = {0};
      sprintf(returnTime, "%.2f", counter * checkTime);
          
      StaticJsonDocument<DEFAULT_JSON_BUFFER_SIZE> returnDoc;
      JsonObject object = returnDoc.to<JsonObject>();
      object["Time"] = returnTime;
      object["dis"] = String(dis);       

      serializeJson(returnDoc, Serial);
      serializeJson(returnDoc, SerialBT);
}

void calculateData()
{

  byte myCheckSum = header + dis_l + dis_h + err;
  if(myCheckSum == checkSum)
  {  
      dis = dis_l + dis_h * 256;
      good++;
      
      //Serial.println("aaa");    
  }else
  {

    bad++;
    }

    Serial.println(good);
    Serial.println(bad);
    float a = good / (good + bad);
   Se


rial.println(a);
      
  state = 0;
}

void checkState(const char* state, const char* checkTimeString)
{
  if(strcmp(state, "ON") == 0)
    {
      checkTime = atof(checkTimeString);
       
      if(checkTime < 0)
        checkTime = DEFAULT_TIME;
          
      active = ON;
    }
    
    if(strcmp(state, "OFF") == 0)
    {
      active = OFF;
      counter = 0;
    }  
}

void initData(byte incomingByte)
{
  //Serial.println(incomingByte, HEX);
    
  if (incomingByte == HEADER && state == HEADER_STATE)
  {
    state = DIS_L_STATE;
    header = incomingByte;
  }

  else if (state == DIS_L_STATE)
  {
    state = DIS_H_STATE;
    dis_l = incomingByte;
  }

  else if (state == DIS_H_STATE)
  {
     state = ERROR_STATE;
     dis_h = incomingByte;
  }

  else if (state == ERROR_STATE)
  { 
    state = CHECK_SUM_STATE;
    err = incomingByte;
  }

  else if (state == CHECK_SUM_STATE)
  {
    state = END_STATE;
    checkSum = incomingByte;
  }

  else if(state == END_STATE)
  {
    calculateData();
  }
}




     
  
